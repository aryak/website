---
pagetitle: Projects
---

# Projects maintained by Arya

I maintain/develop a few projects:

- [Project Segfault](https://projectsegfau.lt) \- a project that hosts many free software privacy frontends and services
- [PublAPI](https://github.com/ProjectSegfault/publapi) \- a simple gofiber web api that manages signups and userinfo for the projectsegfault pubnix
- [Mozhi](https://codeberg.org/aryak/mozhi) \- an alternative-frontend for many translation engines
- [Prav Android](https://codeberg.org/prav/prav) \- see: prav.app; based on conversations.im / quicksy.im
- [This website's static site generator](https://codeberg.org/aryak/website) \- A janky bash script that generates this site's html and rss feeds

## Currently un-maintained projects
- [Gothub](https://codeberg.org/gothub/gothub) \- an alternative-frontend to Github.
- [Periodic Matrix](https://codeberg.org/aryak/periodicmatrix) \- a simple bash script to create 118 users which join a room, print the element name and leave
- [Sedtrix](https://codeberg.org/aryak/sedtrix.sh) \- a matrix sed bot in bash that actually uses sed (probably insecure and for sure badly written)
