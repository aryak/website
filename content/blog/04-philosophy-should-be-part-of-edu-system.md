---
title: Why philosophy should be part of the education system
date: Sat, 19 August 2023
---

I was recently reading the essay, On Liberty, by John Stuart Mill and I was very intrigued by some of the points brought up by the book.

This led me to think again about something I have been pondering about in and out for ages, the place of philosophy in the educational system.

## The problem

I'll start with the problem. To be honest, this generation is extremely apolitical, and non-politically active.

All they care about is getting good marks, passing 10th/12th and getting a good college/job, while ignoring the big and important question, the state of our nation.

When noone cares about the nation, asatyamev hi jayate (injustice alone will prevail), for there is no person to keep check on the government.

Every person's goal in life is to serve themselves. Me first, everything secondary. This is how humans work.

The government is meant to mediate this but its not a bulletproof entity, its made of people, whose primary goal is to serve themselves.

This is why we need someone to keep the government in check, and unintellectual and aloofness from politics does not help.

This I feel, is excacerbated by the education system, especially here, which encourages rote learning.

## The state of the current education system

This is especially bad with social studies, where it is important to be opinion based, not "I just memorized this chapter and can get 100 in tommorow's exam".

I guess this could apply to the sciences, mathematics etc. but it is even more important in the social sciences and humanities.

Social Studies must be opinions, what you think about this thing, who is in the right and who is in the wrong according to you, with multiple sources to understand from.

People should be marked based on the intellectual-ness of the answer, not based on how "politically-correct" or similar to textbook the answer is.

Here is where philosophy plays an important role.

While social studies only covers laws, history and generally how our current system came to be, philosophy questions the basic theory.

Philosophy awakens the intellectual-ness, and makes people think critically.

It encourages and makes people go into deep thoughts and intellectual discussions, which benifit them mentally and society in large as we get more intelligent people.

This encouragement that used to be prevailent in the ancient times of Gurukuls has been effectively wiped off by the advent of the structured educational system.

## My ideas for how philosophy could be implemented in education

Philosophy is a vast and varied subject.

I would say, extracts from philosophical texts from the big 4 traditions (western, indian, chinese, islamic) should be given to students about a specific topic, which of course must be suitable for their grade and they should be asked to comment on what they think about each.

In my opinion, everything should be marked based on the intellect of the answer, and when marks are deducted for "wrong-think", students should be able to appeal to a board setup to answer and co-ordinate with schools about these kind of marking conflicts.

## The "untouchable" subjects

There are many subjects that are untouchable in any normal debate, be it caste, morality, religion etc.

This is BAD and leads to forcing of opinions and generally lack of intellectualism around these topics.

You cannot and should not take a blind follow approach to anything. Question every single thing you have to do, be it rhetorical or directed towards someone else.

## The law

The law should be more liberal when it comes to "wrong-think". Speech should be allowed as long as it does not incite or promote violence.

I know incite violence is vague as hell, and that is of course because of how varied stuff can cause people to get mad.

A small social media post about something inciteful can cause more harm than someone saying extremely inciting/dangerous things to even a big audience.

"Hate speech" can not be regulated, and the intentions of something cannot be easily made out.

To be frank, I don't have any ideas about this. Maybe a committee to decide what constitutes a hate-speech and what does not, but then there are of course biases.

If you go with a simple "if there is violence due to your post we arrest you", you risk arresting people whose post unintentionally incited the same.

But, at the end of the day, the law should be more allowing for free thinking and intellectual discussions, as long as of course, it doesnt incite violent acts.

## Societal stuff

People also are moulded more and more into non-individualistic robots, who all share the same happiness and same opinions.

People like something just because others like it, and if they don't like it they are treated like outcasts from society.

People have no individualistic goals in life, just the standard "getting good marks, passing 10th/12th and getting a good college/job"

While these are also extremely important, we need to stress on the importance of a life where people aren't just robots who do what they are told to do (or NPCs as the zoomers call it).

This could be lessened by philosophy, which imbibes critical thinking in the mind of those who read it.


I should have published this on Independence day since well a lot of these were ponderings about the Indian independence and what it gave us Indians, but well procrastination :)

I know this is a bit rambly but I hope you get the point :P
