---
title: Streaming our FOSS Meetups using just free software!
date: Thu, 17 October 2024
---

I volunteer for [FOSS United Mumbai](https://fossunited.org/c/mumbai), and we organize meetups related to FOSS every month.

We have often done some hacky jugaad to stream our meetups, but due to their last minute nature and lack of proper resources, they have often been low quality or failed entirely.

With the on-ground experience I have gained as the live streaming lead at [IndiaFOSS 2024](https://fossunited.org/indiafoss/2024) and [2023](https://indiafoss.net/2023), I wanted to put these ideas into a blog-post format, so this could help out the [other chapters of FOSS United](https://fossunited.org/city-communities), and potentially non-FOSS United events too!

My guide is primarily  divided into four parts:

- Scrcpy, which deals with getting the video of the meetup by (ab)using the camera of your phone
- VDO.ninja, which retrieves a high quality, low-latency stream of the speaker's screen in a user-friendly manner
- OBS, which combines the outputs of both Scrcpy and VDO.ninja together with a nice template, to send to the streaming server (OSP, or perhaps YouTube)
- OSP (FOSS United specific), which is the web UI which we stream to, and use to make clips out of the live stream

A large part of the guide assumes you use linux, but it should work with MacOS. PS: if you are hosting a FOSS Meetup, streaming from a Mac/Windows Box doesn't make much sense either way :-)

## Scrcpy Setup

Requirements:

- Laptop with Linux/MacOS (only tested with linux though)
- Scrcpy 2.x
- Android 12+ phone

On the phone:

- Enable Developer Options by tapping Build Number 7 times in the Software Information section of About Phone in the Settings app.
    ![](/static/blog/photo_2024-10-17_23-01-56.jpg)
- Go to the newly enabled developer options menu, allow USB Debugging.
    ![](/static/blog/photo_2024-10-17_23-01-59.jpg)
- Connect phone to the laptop via USB, and authorize the Laptop
    - If there is no option asking you to authorize, there should be a silent notification in your notif tray regarding USB preferences. Click that and select ADB (if not available, chose file transfer - that should be an equivalent option)
    ![](/static/blog/photo_2024-10-17_23-01-52.jpg)
    -  It might also be that the cable is old/damaged, and does not support data transfer. If previous option didn't yield any results, try using a different cable

On the laptop:

- Install ADB
- Install Scrcpy ([Linux](https://github.com/Genymobile/scrcpy/blob/master/doc/linux.md) | [MacOS](https://github.com/Genymobile/scrcpy/blob/master/doc/macos.md) | [Windows](https://github.com/Genymobile/scrcpy/blob/master/doc/windows.md))
- Run this command: `/usr/local/bin/scrcpy --video-source=camera --camera-id=0 --audio-source=mic --orientation=90`
    - Sometimes the ideal resolution and framerate will not be the perfect 16:9 one that is required. In that case, add the following argument: `--camera-size=1920x1080 --camera-fps=60`
    - If you want to add any custom settings, consult [this other blogpost of mine](https://aryak.me/blog/06-phone-webcam-scrcpy.html) or [the scrcpy docs](https://github.com/Genymobile/scrcpy/blob/master/doc/camera.md)
    - If you want a landscape camera output, remove the --orientation flag
    - Setting the camera ID to 0 should use the back camera by default, but if front camera needs to be used, set the ID to 1
- This should open a window with a full-sized preview of the camera:
![](/static/blog/photo_2024-10-17_23-02-02.jpg)

## VDO.Ninja / Screenshare Setup

Requirements:

- A browser which supports screenshare on the speaker's PC, any browser on the streaming PC

The Setup:

- On streaming PC, open vdo.ninja, and create a room. Give a valid room name, and set the option select the "The guests can see the director, but not other guests' videos" option, and then enter the control centre
![](/static/blog/photo_2024-10-17_23-02-05.jpg)
- From the control centre, copy the link to invite a guest, and ask the next speaker to open it on his laptop.
![](/static/blog/photo_2024-10-17_23-02-11.jpg)
- On the speaker's laptop, select screenshare with room, and then in the settings cogweel, make it use highest quality.
![](/static/blog/photo_2024-10-17_23-02-13.jpg)
- Back on the streaming PC in the control centre, press highlight on the newly appeared preview, and then copy link of "capture a group scene". Keep the link safe, its needed in the next OBS setup
![](/static/blog/photo_2024-10-17_23-02-16.jpg)

## Open Streaming Platform Setup (specific to FOSS United)

On https://stream.fossunited.org, each city chapter/foss club can request an account for streaming and uploading their talks. Login to the account that is provided to you, navigate to My Channels and create a new channel.

Add the specific details like default title, description, profile picture etc., and then copy the auto-generated stream key.

If you want to stream simultaneously to youtube, you can add a new RTMP Restream Destination in this format: `rtmp://a.rtmp.youtube.com/live2/<STREAM KEY>`, and enable the new destination. By doing this, all new streams will be simultaneously streamed to YouTube as well.
![](/static/blog/photo_2024-10-17_23-26-05.jpg)

### Making a clip for each talk

After the live stream has ended, you might want to separate the stream into smaller chunks, with a separate video for each of the talks. This can be done using the clips feature of OSP.

Once the stream ends, the stream will be converted into a recording and uploaded on to the channel from where it was being streamed.

Open the video, and in it the cogweel, from where you can select the create clip option. Using the slider, move to the desired start and end point, give it a valid description and title, and then create the clip. A video is attached for reference:

![](/static/blog/osp-clip.mp4)

## OBS Setup

Requirements:

- OBS Installation, with the default browser plugin enabled (the plugin is not enabled on debian as of writing FYI, so use the flatpak version if on Debian)
- An X11 environment (if under wayland, run `env -u WAYLAND_DISPLAY obs`

- Run setup wizard, optimize for streaming, and use 1920x1080 as the base resolution. For stream key, enter the one from your service provider (if you are using OSP, consult the next section)
- Disable MIC/AUX, and keep Desktop Audio enabled
- In OBS, add a window source (Labelled XComposite Window Capture on linux) for scrcpy
- In OBS, add a browser source, and make the url point towards the link of the "capture group scene" from the VDO.ninja setup.
- At the end, it should look something like this:
![](/static/blog/photo_2024-10-17_23-02-18.jpg)
- Start streaming!
