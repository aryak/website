---
title: Using my phone as a webcam with just scrcpy and ADB
date: Sat, 27 January 2024
---

Recently, we decided to start streaming [FOSS United Mumbai](https://fossunited.org) events to our [Peertube instance](https://sovran.video), and hence we needed a camera.

Since we couldn't procure a good camera that can capture text from the projector well, I started experimenting with other alternatives.

The first solution I tried was a generic IP camera based solution, which of course came with the latencies involved with network-based stuff and hence wasn't suitable for this purpose.

Then, I tried droidcam, which uses ADB, but again, it had a paywall for anything over 480p.

Past this, my _jugaad_ solution was to use scrcpy via USB ADB, open the normal camera app and then just capture part of the window with the content, but that came with the issue of only being able to use 4:3 aspect ratio, and potentially worse quality since liveview is rarely as good as actuals.

Later, I discovered that scrcpy can natively capture the phone camera since Android 12+ and Scrcpy 2.x+.

This came with the first problem, debian **testing** is still stuck on 1.x of scrcpy! 

Note to self: maybe I should try porting a 2.x to Debian :D

As outlined in the [docs](https://github.com/Genymobile/scrcpy/blob/master/doc/camera.md), scrcpy can capture both front and back cameras, at all supported resolutions, and even the audio from the phone's mic (which is useful considering how shitty the mics are on modern laptops these days) .

So at the end, I ended up with this command:

```bash
/usr/local/bin/scrcpy --video-source=camera --camera-id=0 --camera-size=3264x1836
```

Now, at this point, I just needed to run OBS under Xwayland, add it as an Xcomposite video capture, and start streaming!

But then, if you want to use it as a webcam, you just have to install v4l2loopback (and run modprobe v4l2loopback), and then click "Start Virtual Camera" instead of Start Streaming.

Do note though, you might have to change output type to scene under the settings section right next to the start virtual camera button.

But thats about it. This is still a bit jank, considering you need scrcpy and OBS both running in the background, but it generally does the job pretty well, and a good phone's webcam is miles better than any other mid-range web-cam you can get.
