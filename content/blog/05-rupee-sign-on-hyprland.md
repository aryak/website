---
title: Enabling AltGr+4 for rupee/euro on Hyprland
date: Fri, 3 November 2023
---

Enabling the rupee sign (or the euro sign for that matter) on Hyprland is pretty simple, but not well documented from my "research".

To begin with, you need to use the altgr-intl layout in order to be able to use it in the first place. This also gives you access to [many other characters](https://raw.githubusercontent.com/google/us-altgr-intl/master/images/keyboard_english_us_intl.png) as well.

To do this, add `kb_variant = altgr-intl` in the input section of your `hyprland.conf`.

Past this, the configuration is pretty simple, you just have to add the required options to `kb_options`.

You can get a list of these with `localectl list-x11-keymap-options`.

In my case, I needed `rupeesign:4`

At the end, this is how the hyprland.conf's input section looks:

```
input {
    kb_layout = us
	kb_options = rupeesign:4, caps:backspace
	kb_variant = altgr-intl
	[...mouse stuff...]
}
```
