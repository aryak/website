---
title: Linux on the Thinkpad E14 G5 AMD
date: Wed, 16 August 2023
---

I recently got a thinkpad E14 gen5 with a ryzen 5 7th gen to replace my Acer Aspire 7.

Of course I wanted to replace the default windows with linux, but I was too lazy to reinstall and hence just put my old NVMe SSD in the laptop thanks to the availability of 2 M.2 slots.

Before this I had to disable secure boot in the bios which I did along with a few other "important" changes (which I'll cover later)

Past this, most things worked out of the box after removing my old Nvidia drivers. Ryzen is really good on linux :P

However, 2 things didn't work correctly, the Fingerprint sensor and WiFi card.

## Realtek WiFi
After the initial setup, my realtek wifi card was having frequent disconnection issues. It will suddenly just stop transmitting data and the fix was to reconnect to the wifi network. 

Another issue is that the Realtek WiFi card only works with kernels 6.2+. This wasn't an issue for me as a Debian Sid user but when I first tested the compatibility on an Ubuntu 22.04 ISO before adding my drive, this was pretty weird/confusing.

After a bit of searching, I figured out that the random disconnection was due to NetworkManager's randomized mac addresses. 

To fix this, I just had to append the following to the NetworkManager.conf file:
```
[device]
wifi.scan-rand-mac-address=no
```

## Goodix Fingerprint
I thought the fingerprint was useless for good, until I discovered that Lenovo provided drivers for their fingerprint sensor through one of the manuals I was searching through.

Originally I didn't find anything, since the driver page for my model didn't show linux, but after a bit of searching again, I stumbled across [this support page](https://support.lenovo.com/in/en/downloads/ds560884-goodix-fingerprint-driver-for-linux-thinkpad-e14-gen-4-e15-gen-4) that gave links to drivers. 

Since I ran debian testing and its mostly compatible with what Ubuntu has, I tried just installing libfprint-2-2 normally and then the driver's deb, but the deb refused to install since it needed [libfprint-2-tod](https://gitlab.freedesktop.org/3v1n0/libfprint/-/tree/tod?ref_type=heads) (a fork of libfprint with support for TOD/touch based fingerprint readers).

I tried installing the deb from ubuntu repos and then install the driver, which succeeded, but didn't make the device work.

Later, I installed ubuntu 22.04 on a secondary partition to check if it worked on ubuntu, which it did.

One thing I noticed, however was that it used [a ppa](https://launchpad.net/~andch/+archive/ubuntu/staging-fprint/+packages) instead of regular libfprint(-tod) from canonical repos.

Then, I tried installing libfprint and libfprint-tod debs from that ppa and the goodix deb from lenovo, and after a reboot, everything worked!

Once it was up, I installed fprintd and libpam-fprintd, at which point I did the fprintd-enroll and fprintd-verify.

After these succeeded, I ran `pam-auth-update --enable fprintd`. This made fprintd work on pam stuff like sudo and TTY logins.

With this, the last remaining non-working feature of my thinkpad was working as well :D

## The BIOS
Once I got my thinkpad, basically the first thing I did was tweak the BIOS.

Getting into the BIOS is dead simple compared to other laptops, just press enter (which it tells you during bootup) and it will take you to a menu, from where you can choose to go to the BIOS with F1.

I was pleasantly surprised to see a modern BIOS, with touchpad support. Coming from laptops which only had blue and white bioses and barely any  tweaks, this was a welcome upgrade :)

First thing to disable was secure boot. It was present in Security section of the BIOS and was pretty easy to disable

Then, I permanently disabled some enterprise spyware utility (update: it was called Absolute under Security). 

There was also Lenovo Cloud Services in Config -> Network, which I of course disabled.

While in Security, I disabled Enhanced Windows Biometric Security (in Virtualization) and Microsoft Device Guard.

And thats it for now, I'll update this blogpost if I face any other issues or discover other stuff. Thanks for sticking around :)

If you have any questions, feel free to [contact me](/contact)!
