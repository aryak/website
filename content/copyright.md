---
pagetitle: Copyright
---

# Website Copyright information

All content on this website is licensed under [CC0](https://creativecommons.org/share-your-work/public-domain/cc0) ([locally hosted](/LICENSE-content.txt)).

Additionally, content on the blog ([/blog](/blog)) is licensed under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) ([locally hosted](/LICENSE-blog.txt)).

The site generator is mostly borrowed from [git.vern.cc:pjals/in.vern.cc](https://git.vern.cc/pjals/in.vern.cc), which is [AGPLv3](/LICENSE-sitegen.txt),
