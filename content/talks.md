---
pagetitle: Talks
---

# A (incomplete) list of the conferences/meetups I have spoken at

- FOSSMeet'23 NIT Calicut \- [Free Software, Decentralization, Self-Hosting and Free Service Hosts](https://aryak.me/fossmeetalk/)
- FOSS United Mumbai June Meetup \- [A brief "review" of FOSS Instant Messengers](https://aryak.me/mum-fm-jun23)
- IndiaFOSS 3.0 \- [Self-Hosting Workshop](https://i10e.xyz)
- FOSS United Chennai November Meetup \- [Prav - A privacy focused messenger for the users, of the users, and by the users](https://aryak.me/chennai-fm-nov23)
- FOSS United Mumbai December Meetup \- [An introduction to ansible](https://aryak.me/mum-fm-dec23)
- MumbaiFOSS 2.0 \- [Geo-steered load balancing on a budget](https://aryak.me/mumbaifoss2.0talk) ([recording](https://www.youtube.com/watch?v=aZQZGJ-uSDg))

If you have any questions regarding any talk I did, feel free to [reach out](https://aryak.me/contact)!
