---
pagetitle: Contact
---

# How to contact Arya

**You do not have to use non-free software to contact me**

Here are some ways to contact me, listed in preferential order.

- E-Mail - me &lt;at symbol&gt; aryak.me
- Matrix: [@arya:frei.chat](https://matrix.to/#/@aryak:projectsegfau.lt)
- XMPP: [arya@projectsegfau.lt](xmpp:arya@projectsegfau.lt) (OMEMO preferred over PGP)

**Infrequently checked contact methods**

- IRC - `aryak` on tilde.chat (#meta), LiberaChat (#libera).
- Fediverse - [@arya@social.linux.pizza](https://social.linux.pizza/@arya)

## Encryption
I encourage use of strong encryption when trying to contact me.

For PGP (E-Mail/XMPP) you can find my key at [keys.openpgp.org](https://keys.openpgp.org/search?q=arya%40projectsegfau.lt) or [/pgp.asc](/pgp.asc)
