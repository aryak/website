---
pagetitle: Main
---

# Welcome to Arya's website!

I am a [Libre Software](https://gnu.org/philosophy/open-source-misses-the-point.html) Enthusiast and student from Chennai, living in Mumbai, India. I am also one of the sysadmins of [Project Segfault](https://projectsegfau.lt/team)

Outside of software freedom and technology, I am interested in History, Geography and Biology.

I mostly only use libre software and mainly use my Thinkpad E14 running Debian Testing, and my Pixel 6a running GrapheneOS.

## Blog ([RSS](/rss.xml))

BLOG_REPLACE

## Other cool websites (doesn't imply this site is cool :P)

- [AndrewYu](https://andrewyu.org)
- [Carlos Fenollosa](https://cfenollosa.com)
- [Gtlsgamr](https://hitarththummar.xyz)
- [Nixcraft](https://cyberciti.biz)
- [Pistasjis](https://pistasjis.net)
- [William](https://willy.club) (Thanks for helping with CSS!)
- [Xmoo](https://xmoo.tilambda.zone)
- [Ravi Dwivedi](https://ravidwivedi.in)
- [Sahilister](https://sahilister.in)
- [Rushabh Mehta](https://rushabh-mehta.medium.com)
- [Kailash Nadh](https://nadh.in)
- [Contrapunctus](https://contrapunctus.codeberg.page)
- [Pranav Chakkarwar](https://pra9.com)
- [Midou](https://midou.dev)
- [Mangesh](https://mangeshm.xyz)
- [K Gopal Krishna](https://kayg.org)

<small>Needless to say, I do not unconditionally support the contents of the above websites.</small>
