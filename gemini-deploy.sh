#!/usr/bin/env bash
shopt -s globstar
shopt -s extglob
# Core Vars
SITE_SRC=${PWD}
SITE_HTML_OUT=${SITE_SRC}/dist
SITE_OUT=${SITE_SRC}/gemdist
rm -fr $SITE_OUT/* $SITE_OUT/.*
# Copy static assets to dist
cp -r ${SITE_SRC}/static ${SITE_OUT}
cp -r ${SITE_SRC}/LICENSE-* ${SITE_OUT}
cp -r ${SITE_SRC}/style.css ${SITE_OUT}
cp -r ${SITE_SRC}/otherfiles/* ${SITE_OUT}
sed -i 's/^- //g' content/index.md
for d in $SITE_SRC/content/**/; do
	NEW_PATH=$SITE_OUT/$(realpath --relative-to=$SITE_SRC/content $d)
	mkdir -p $NEW_PATH
done
for f in $SITE_SRC/content/**/*.md; do
	NEW_PATH=$SITE_OUT/$(realpath --relative-to=$SITE_SRC/content $f)
	NEW_PATH=${NEW_PATH%".md"}.gmi
	if [[ $NEW_PATH =~ "blog" ]]; then
		SITE_OUT=${SITE_OUT}/blog
	fi
	gemgen -e markdown -r '' -o $SITE_OUT "${f}"
	sed -i 's/pagetitle: //g' $NEW_PATH
	sed -i 's/title: //g' $NEW_PATH
	cat header.gmi $NEW_PATH footer.gmi | tee $NEW_PATH > /dev/null
	SITE_OUT=${SITE_SRC}/gemdist
done
cp -r $SITE_HTML_OUT/rss.xml $SITE_OUT
sed -i 's/https\:\/\/aryak.me/gemini\:\/\/p.projectsegfau.lt\/\~arya/g' $SITE_OUT/rss.xml
sed -i 's/.html/.gmi/g' $SITE_OUT/index.gmi

