#!/usr/bin/env bash
shopt -s globstar
shopt -s extglob
# Core Vars
SITE_SRC=${PWD}
SITE_OUT=${SITE_SRC}/dist
# RSS Vars
DOMAIN=https://aryak.me
DESCRIPTION="The blog of Arya"
COPYRIGHT="Copyright 2022-23, Arya Kiran"
AUTHOR="arya@projectsegfau.lt (Arya Kiran)"
TTL=60

rssgen() {
echo "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>
<rss version=\"2.0\" xmlns:atom=\"http://www.w3.org/2005/Atom\">
  <channel>
    <title>Arya's blog</title>
    <link>$DOMAIN</link>
    <description>$DESCRIPTION</description>
    <copyright>$COPYRIGHT</copyright>
    <ttl>$TTL</ttl>
	<atom:link href=\"$DOMAIN/rss.xml\" rel=\"self\" type=\"application/rss+xml\" />";

for file in $(ls -1 $SITE_OUT/blog/*.html | sort -r); do
	POST_DATE=$(sed -n 's|^<p class="date">\([^<]*\)</p>$|\1|p' $file)
	POST_TITLE=$(cat $file | htmlq -t h1.title | tr -d '\n')
	POST_CONTENT=$(sed -n '/<article>/,/<\/article>/p' $file | sed -e '1s/.*<article>//' -e '$s/<\/article>.*//')
	CAT_DATE=$(date -u -d "$(sed -n 's|^<p class="date">\([^<]*\)</p>$|\1|p' $file)" +"%Y/%m/%d/%u")
	POST_DATE=$(date -u -d "$(sed -n 's|^<p class="date">\([^<]*\)</p>$|\1|p' $file)" +"%a, %d %b %Y")
echo "<item>
  <pubDate>$POST_DATE $(date -u +"%T %z")</pubDate>
  <category>$CAT_DATE</category>
  <title>$POST_TITLE</title>
  <link>$DOMAIN/blog/$(basename ${file})</link>
  <description><![CDATA[$POST_CONTENT]]></description>
  <author>$AUTHOR</author>
  <guid>$DOMAIN/blog/$(basename ${file})</guid>
  </item>";
done
echo "  </channel>
</rss>";
}

rm -rf $SITE_OUT && mkdir $SITE_OUT
# Copy static assets to dist
cp -r ${SITE_SRC}/static ${SITE_OUT}
cp -r ${SITE_SRC}/LICENSE-* ${SITE_OUT}
cp -r ${SITE_SRC}/style.css ${SITE_OUT}
for d in $SITE_SRC/content/**/; do
	NEW_PATH=$SITE_OUT/$(realpath --relative-to=$SITE_SRC/content $d)
	mkdir -p $NEW_PATH
done
# Add blog posts inline (very hacky)
rm -rf /tmp/blogdata
for i in $(ls -1 $SITE_SRC/content/blog/*.md | sort -r); do
	title=$(cat $i | grep -Po '(?<=title: ).*')
	date=$(cat $i | grep -Po '(?<=date: ).*')
	filename=$(basename $i)
	cp $SITE_SRC/index.md $SITE_SRC/content
	echo "- [_${date}_ \- ${title}](/blog/${filename%".md"}.html)" >> /tmp/blogdata
done
sed -i $'/BLOG_REPLACE/{e cat     /tmp/blogdata\n}' $SITE_SRC/content/index.md
sed -i s/BLOG_REPLACE// $SITE_SRC/content/index.md
# Actual site gen
for f in $SITE_SRC/content/**/*.md; do
	NEW_PATH=$SITE_OUT/$(realpath --relative-to=$SITE_SRC/content $f)
	NEW_PATH=${NEW_PATH%".md"}.html
	pandoc --css /style.css \
		-f markdown -t html5 \
		-T 'Arya' --highlight-style ${SITE_SRC}/pygments.theme \
		-A $SITE_SRC/footer.html -B $SITE_SRC/header.html \
		--template=${SITE_SRC}/template.html5 \
		$f > $NEW_PATH
done
# Add comments only on le blog
sed -i $'/<footer>/{e cat     ./comments.html\n}' $SITE_OUT/blog/*
# RSS
rssgen > $SITE_OUT/rss.xml
cp -r ${SITE_SRC}/otherfiles/* ${SITE_OUT}
# Gemini
#./gemini-deploy.sh
